<?php

class Users extends CI_controller{
	public function register(){
		//validation rules
		$this->form_validation->set_rules('first_name','First_Name','trim|required|min_length[2]|max_length[10]');		
		$this->form_validation->set_rules('last_name','Last_Name','trim|required|min_length[2]|max_length[10]');		
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');		
		$this->form_validation->set_rules('username','Username','trim|required|min_length[2]|max_length[10]');		
		$this->form_validation->set_rules('password','Password','trim|required|min_length[8]|max_length[50]');		
		$this->form_validation->set_rules('password2','Confirm Password','trim|required|matches[password]');		
		if($this->form_validation->run() == FALSE){
			$data['main_content'] = 'register';
			$this->load->view('layouts/main', $data);
		}else{
			if($this->User_model->register()){
				$this->session->set_flashdata('registered','Congratulations! You can now login');
				redirect('products');
			}
		}
	}

	public function login(){
		$this->form_validation->set_rules('username','Username','trim|required');		
		$this->form_validation->set_rules('password','Password','trim|required');
		//$_POST['username']
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$user_id = $this->User_model->login($username, $password);
		//vslidate user
		if ($user_id) {
		  	$data = array(
		  		'user_id'=>user_id,	
		  		'username'=>username,	
		  		'logged_in'=>true
		  		);
		 	//session for userdata
		 	$this->session->set_userdata($data);
		 	$this->session->set_flashdata('pass_login', 'Welcome '.$username.' !');
		 	redirect('products');	
		  		}else{
		  			//error
		  			$this->session->set_flashdata('fail_login','Login Failed :(');
		  			redirect('products');
		  	}  				
	}

	public function logout(){
		//unset userdata
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('username');
		$this->session->sess_destroy();
		redirect('products');
	}
}




