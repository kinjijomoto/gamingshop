<?php if($this->cart->contents()) : ?>
	<form method="post" action="<?php echo base_url(); ?>cart/process">
		<table class="table table striped">
			<tr>
				<th>Quantity</th>
				<th>Item title</th>
				<th style="text-align:right">Item price</th>
			</tr>
			<?php $i= 0; ?>
			<?php foreach ($this->cart->contents() as $items): ?>
				<tr>
					<td><?php echo $items['qty']; ?></td>
					<td><?php echo $items['name']; ?></td>
					<td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
				</tr>
				<?php echo '<input type="hidden" name="item_name['.$i.']" value="'.$items['name'].'">'; ?>
				<?php echo '<input type="hidden" name="item_code['.$i.']" value="'.$items['id'].'">'; ?>
				<?php echo '<input type="hidden" name="item_qty['.$i.']" value="'.$items['qty'].'">'; ?>
			<?php $i++; ?>	
			<?php endforeach; ?>
				<tr>
					<td class="right"><strong>Shipping</strong></td>
					<td class="right" style="text-align:right">$<?php echo $this->config->item('Shipping'); ?></td>
				</tr>
				<tr>
					<td class="right"><strong>Tax</strong></td>
					<td class="right" style="text-align:right">$<?php echo $this->config->item('tax'); ?></td>
				</tr>
				<tr>
					<td class="right"><strong>Total</strong></td>
					<td class="right" style="text-align:right">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
				</tr>					
		</table>
		<?php if($this->session->userdata('logged_in')) : ?> 
			<div class="form-group">
				<label>Address</label>
				<input type="text" class="form-control" name="address">
			</div>
			<div class="form-group">
				<label>Address2</label>
				<input type="text" class="form-control" name="address2">
			</div>
			<div class="form-group">
				<label>City</label>
				<input type="text" class="form-control" name="city">
			</div>
			<div class="form-group">
				<label>State</label>
				<input type="text" class="form-control" name="state">
			</div>
			<div class="form-group">
				<label>Zipcode</label>
				<input type="text" class="form-control" name="zipcode">
			</div>
			<p><button class="btn btn-primary" type="submit" name="submit">Checkout</button></p>
		<?php endif; ?>
	</form>
<?php else : ?>
	<p>There is no item in your cart</p>
<?php endif; ?>	